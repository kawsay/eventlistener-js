// ==========    1    ==========
let num = 0;
let footer = document.getElementsByTagName('footer')[0];
footer.addEventListener('click', function() {
  num = num + 42;
  console.log('CLICK' + num);
});


// ==========    2    ==========
let navbarHeader = document.getElementById('navbarHeader');
let navbarToggler = document.getElementsByClassName('navbar-toggler')[0];
let clicked = false;
navbarToggler.addEventListener('click', function() {
  if (clicked === false) {
    clicked = true;
    navbarHeader.classList.remove('collapse');
  } else {
    clicked = false;
    navbarHeader.classList.add('collapse');
  }
});



// ==========    3    ==========
let editButton1 = document.querySelector('div.btn-group > button:last-child');
let cardText1 = document.querySelector('div.card-body > p');
editButton1.addEventListener('click', function() {
  cardText1.classList.add('text-danger');
});




// ==========    4    ==========
let editButton2 = document.querySelector('.col-md-4:nth-of-type(2) button:nth-child(2)');
let cardText2 = document.querySelector('.col-md-4:nth-of-type(2) div.card-body > p');
let editButton2Clicked = false;
editButton2.addEventListener('click', function() {
  if (editButton2Clicked === false) {
    editButton2Clicked = true;
    cardText2.classList.add('text-success');
  } else {
    editButton2Clicked = false;
    cardText2.classList.remove('text-success');
  }
});




// ==========    5    ==========
let navbar = document.querySelector('.navbar');
let bootstrapCDN = document.querySelector('link');
let navbarClicked = false;
navbar.addEventListener('dblclick', function() {
  if (navbarClicked === false) {
    navbarClicked = true;
    bootstrapCDN.remove();
  } else {
    navbarClicked = false;
    document.querySelector('header').append(bootstrapCDN);
  }
});





// ==========    6    ==========
let viewButtons = document.querySelectorAll('.col-md-4 button:first-child');
let cardsText = document.querySelectorAll('.col-md-4 p');
let cardsImg = document.querySelectorAll('.col-md-4 img');
for (let i = 0; i < viewButtons.length; i++) {
  viewButtons[i].addEventListener('mouseover', function() {
    if (getComputedStyle(cardsImg[i]).maxWidth !== '20%') {
      cardsImg[i].style.maxWidth = "20%";
      cardsText[i].classList.add("collapse");
    } else {
      cardsImg[i].style.maxWidth = "100%";
      cardsText[i].classList.remove("collapse");
    }
  });
}





// ==========    7    ==========
let rightArrow = document.getElementsByClassName('my-2')[1];
let cards = document.getElementsByClassName('col-md-4');
let row = document.getElementsByClassName('row')[1];
rightArrow.addEventListener('click', function() {
  row.insertBefore(cards[cards.length - 1], cards[0]);
});





// ==========    8    ==========
let leftArrow = document.getElementsByClassName('my-2')[0];
leftArrow.addEventListener('click', function(event) {
  event.preventDefault();
  row.insertBefore(cards[0], cards[cards.length - 1]);
});





// ==========    9    ==========
let navbarBrand = document.getElementsByClassName('navbar-brand')[0];
let body = document.getElementsByTagName('body')[0];
navbarBrand.addEventListener('keypress', function(event) {
  body.classList = "";
  switch (event.key) {
    case 'a':
      body.classList.add("col-4");
      break;
    case 'b':
      body.classList = "";
      break;
    case 'y':
      body.classList.add("col-4", "offset-4");
      break;
    case 'p':
      body.classList.add("col-4", "offset-8");
      break;
  }
});

